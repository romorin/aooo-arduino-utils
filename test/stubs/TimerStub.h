#ifndef TIMER_STUB_H
#define TIMER_STUB_H

#include "Timer.h"

class TimerStub: public Timer
{
public:
	TimerStub() {}
	virtual ~TimerStub() {}

	void resetTime()
	{
		_receivedTime = 0;
	}

	virtual bool attach_ms(const unsigned long milliseconds, std::function<void ()> callback)
	{
		_receivedTime = milliseconds;
		_receivedFunction = callback;

		return true;
	}
	virtual bool attach_us(const unsigned long microseconds, std::function<void ()> callback)
	{
		_receivedTime = microseconds;
		_receivedFunction = callback;

		return true;
	}
	virtual bool once_ms(const unsigned long milliseconds, std::function<void ()> callback)
	{
		_receivedTime = milliseconds;
		_receivedFunction = callback;

		return true;
	}
	virtual bool once_us(const unsigned long microseconds, std::function<void ()> callback)
	{
		_receivedTime = microseconds;
		_receivedFunction = callback;

		return true;
	}

	std::function<void ()> receivedFunction()
	{
		return _receivedFunction;
	}
	unsigned long receivedTime()
	{
		return _receivedTime;
	}


private:
	std::function<void ()> _receivedFunction;
	unsigned long _receivedTime;
};

#endif
