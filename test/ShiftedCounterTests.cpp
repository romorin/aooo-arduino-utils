#include "catch2/catch.hpp"

#include "ShiftedCounter.h"

#include "stubs/PinsStub.h"
#include "stubs/TimerStub.h"

SCENARIO( "ShiftedCounter works" ) {
	GIVEN( "A ShiftedCounter and some stubs" ) {
		PinsStub serialDataPin;
		PinsStub inputClockPin;
		PinsStub outputClockPin;
		TimerStub timer;

		const unsigned char maxCount = 2;
		const unsigned long hiPeriod = 3;

		ShiftedCounter counter(serialDataPin, inputClockPin, outputClockPin,
			timer, maxCount, hiPeriod);

		WHEN( "The counter is not ready" ) {
			THEN( "Does not increment" ) {
				REQUIRE( counter.currentCount() == 1 );
				counter.increment();
				REQUIRE( counter.currentCount() == 1 );
			}

			AND_THEN( "Setup is initiated" ) {
				REQUIRE( timer.receivedTime() == hiPeriod );
				REQUIRE( serialDataPin.get() == 1 );
				REQUIRE( inputClockPin.get() == 0 );
				REQUIRE( outputClockPin.get() == 1 );
			}
		}

		AND_WHEN( "unClock is called" ) {
			timer.resetTime();
			timer.receivedFunction()();

			THEN( "Clocks are lowered" ) {
				REQUIRE( inputClockPin.get() == 1 );
				REQUIRE( outputClockPin.get() == 0 );
			}

			AND_THEN( "Data is on" ) {
				REQUIRE( serialDataPin.get() == 1 );
			}

			AND_THEN( "Still not ready" ) {
				REQUIRE( counter.ready() == false );
			}

			AND_THEN( "Called increment" ) {
				REQUIRE( timer.receivedTime() == hiPeriod );
			}
		}

		AND_WHEN( "init is completed" ) {
			timer.receivedFunction()();
			timer.receivedFunction()();
			timer.resetTime();
			timer.receivedFunction()();

			THEN( "is ready" ) {
				REQUIRE( counter.ready() == true );
			}

			AND_THEN( "count is 0" ) {
				REQUIRE( counter.currentCount() == 0 );
			}

			AND_THEN( "increment is not called" ) {
				REQUIRE( timer.receivedTime() == 0 );
			}

			AND_THEN( "Data is off" ) {
				REQUIRE( serialDataPin.get() == 0 );
			}
		}
	}
}
