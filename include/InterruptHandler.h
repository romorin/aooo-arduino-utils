#ifndef INTERRUPT_HANDLER_H
#define INTERRUPT_HANDLER_H

class InterruptHandler
{
public:
	virtual ~InterruptHandler() {}
	virtual void ping() = 0;
};

#endif
