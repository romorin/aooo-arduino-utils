#ifndef DEBUG_PRINT_H
#define DEBUG_PRINT_H

#ifdef AR_DEBUG
	#include <Arduino.h>
	#define DEBUG_BEGIN(x)  Serial.begin(x)
	#define DEBUG_PRINTLN(x)  Serial.println (x)
	#define DEBUG_PRINT(x)  Serial.print (x)
	#define DEBUG_PRINTF(x, y)  Serial.printf (x, y)
#else
	#define DEBUG_BEGIN(x)
	#define DEBUG_PRINTLN(x)
	#define DEBUG_PRINT(x)
	#define DEBUG_PRINTF(x, y)
#endif

#endif
