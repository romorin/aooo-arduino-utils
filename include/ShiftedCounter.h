#ifndef SHIFTED_COUNTER_H
#define SHIFTED_COUNTER_H

#include "Counter.h"
#include "Pins.h"
#include "Timer.h"

#include <functional>

#include "stddef.h"

class ShiftedCounter : public Counter
{
public:
	ShiftedCounter(Pins<char>& serialDataPin,
		Pins<char>& inputClockPin,
		Pins<char>& outputClockPin,
		Timer& timer,
		const size_t maxCount,
		const unsigned long hiPeriod);
	virtual ~ShiftedCounter();

	virtual bool ready() const;
	virtual const size_t currentCount() const;
	virtual void increment();
	virtual size_t maxCount() const;

private:
	void clock();
	void unClock();

	Pins<char>& _serialDataPin;
	Pins<char>& _inputClockPin;
	Pins<char>& _outputClockPin;

	Timer& _timer;

	const size_t _maxCount;
	const unsigned long _hiPeriod;
	size_t _count;
	bool _ready;

	const std::function<void()> _clockFct;
	const std::function<void()> _unclockFct;
};

#endif
