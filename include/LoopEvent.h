#ifndef LOOP_EVENT_H
#define LOOP_EVENT_H

struct LoopEvent {
	unsigned long millis;
	unsigned long micros;
};

#endif
