#ifndef ARDUINO_INTERRUPT_TABLE_H
#define ARDUINO_INTERRUPT_TABLE_H

#include "InterruptHandler.h"
#include "InterruptTable.h"

#include "Arduino.h"

class ArduinoInterruptTable : public InterruptTable
{
public:
	ArduinoInterruptTable();
	virtual ~ArduinoInterruptTable();

	virtual void add(const unsigned char number,
		InterruptHandler& handler, const unsigned char mode);
private:

	// @todo better way?
	static void ICACHE_RAM_ATTR Interrupt_0(void);
	static void ICACHE_RAM_ATTR Interrupt_1(void);
	static void ICACHE_RAM_ATTR Interrupt_2(void);
	static void ICACHE_RAM_ATTR Interrupt_3(void);
	static void ICACHE_RAM_ATTR Interrupt_4(void);
	static void ICACHE_RAM_ATTR Interrupt_5(void);
	static void ICACHE_RAM_ATTR Interrupt_6(void);
	static void ICACHE_RAM_ATTR Interrupt_7(void);
	static void ICACHE_RAM_ATTR Interrupt_8(void);
	static void ICACHE_RAM_ATTR Interrupt_9(void);
};

#endif
