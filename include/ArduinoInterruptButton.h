#ifndef ARDUINO_INTERRUPT_BUTTON_H
#define ARDUINO_INTERRUPT_BUTTON_H

#include "include/Observer.h"
#include "include/Subject.h"
#include "InterruptHandler.h"
#include "InterruptTable.h"
#include "LoopEvent.h"

class ArduinoInterruptButton : public InterruptHandler, public Observer<LoopEvent>
{
public:
	ArduinoInterruptButton(
		const unsigned char pin,
		Observer<bool>& observer,
		InterruptTable& table,
		Subject<LoopEvent>& subject,
		unsigned long bounceTime = 100
	);
	virtual ~ArduinoInterruptButton();

	virtual void ping();
	virtual void update(const LoopEvent& event);

private:
	unsigned char _pin;
	Observer<bool>& _observer;
	unsigned long _bounceTime;

	volatile bool _bounceEvent = false;
	bool _debouncing = false;
	unsigned long _lastDebounceTime;
	unsigned char _buttonState;
};

#endif
