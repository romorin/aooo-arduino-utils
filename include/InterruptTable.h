#ifndef INTERRUPT_TABLE_H
#define INTERRUPT_TABLE_H

#include "InterruptHandler.h"

class InterruptTable
{
public:
	virtual ~InterruptTable() {}
	virtual void add(
		const unsigned char number,
		InterruptHandler& handler,
		const unsigned char mode) = 0;
};

#endif
