#include "TimerImpl.h"

TimerImpl::TimerImpl(Subject<LoopEvent>& subject) : _lastEvent({0,0}),
	_once(false), _ms(false), _period(0), _lastTick(0), _callback(nullptr)
{
	subject.attach(*this);
}

TimerImpl::~TimerImpl() {}

bool TimerImpl::attach_ms(unsigned long milliseconds,
	std::function<void ()> callback)
{
	return fill(false, true, milliseconds, callback);
}

bool TimerImpl::attach_us(unsigned long microseconds,
	std::function<void ()> callback)
{
	return fill(false, false, microseconds, callback);
}

bool TimerImpl::once_ms(unsigned long milliseconds,
	std::function<void ()> callback)
{
	return fill(true, true, milliseconds, callback);
}

bool TimerImpl::once_us(unsigned long microseconds,
	std::function<void ()> callback)
{
	return fill(true, false, microseconds, callback);
}

bool TimerImpl::fill(bool once, bool ms, unsigned long time,
	std::function<void ()> callback)
{
	if (time == 0 || callback == nullptr || isLoaded())
	{
		return false;
	}
	_once = once;
	_ms = ms;
	_period = time;
	_lastTick = _ms ? _lastEvent.millis : _lastEvent.micros;
	_callback = callback;

	return true;
}

void TimerImpl::reset()
{
	_period = 0;
	_callback = nullptr;
}

bool TimerImpl::isLoaded()
{
	return _period != 0;
}

void TimerImpl::update(const LoopEvent& event)
{
	_lastEvent = event;

	if (isLoaded())
	{
		unsigned long time = _ms ? _lastEvent.millis : _lastEvent.micros;

		if (time - _lastTick >= _period)
		{
			if (_once)
			{
				std::function<void ()> callback = _callback;
				reset();
				callback();
			}
			else
			{
				_lastTick = time;
				_callback();
			}
		}
	}
}
